// shuffle.js

function shuffle(orig) {
    var origType = (typeof(orig)).toLowerCase();
    var eMsgEnd = " Cannot be shuffled.";
    var eMsg1 = "Argument is undefined." + eMsgEnd + " (E1)";
    var eMsg2 = "Argument is not an array or string." + eMsgEnd + " (E2)";
    var iMsg1 = "Argument is an empty string. Nothing to shuffle." + " (I1)";
    var iMsg2 = "Argument is an empty array. Nothing to shuffle." + " (I2)";
    var output = [];                    // shuffled array/string to return
    var totalLeft, origLength, rand;                           // random #
    try {
        // check for undefined or arguments w/o .length
        totalLeft = orig.length;
        origLength = orig.length;
    }
    catch(e) {
        if (orig === undefined) { console.log(eMsg1); }
        else { console.log(eMsg2); }
        return false;
    }
    if (origType === "string") { output = shuffleString(orig); }
    else if (origType != 'object') {
        // not a valid type to shuffle
        console.log(eMsg2);
        return false;
    }
    else {
        try {
            // check if an array
            if (orig.length > 0) { /* array is not empty */ }
            else {
                if (orig.length === 0) {
                    // empty array
                    console.log(iMsg2);
                    return orig;
                }
                else {
                    // not an array -- no length property
                    console.log(eMsg2);
                    return false;
                }
            }
        }
        catch(e) {
            // if length property undefined
            console.log(eMsg2);
            return false;
        }
        output = shuffleArray(orig);
    }
    return output;

    function shuffleArray(orig) {
        var newArr = [];
        while (totalLeft > 0) {
            // keep shuffling until original array is empty
            rand = Math.floor(Math.random() * orig.length);
            if (orig[rand] !== undefined) {
                newArr[origLength - totalLeft] = orig[rand];
                orig[rand] = undefined;
                totalLeft--;
            }
            trimEmpties();
        }
        return newArr;
    }

    function shuffleString(orig) {
        var newStr = '';
        if (orig === "") {
            console.log(iMsg1);
            return "";
        }
        while (orig.length > 0) {
            // keep shuffling until original string is empty
            rand = Math.floor(Math.random() * orig.length);
            newStr = newStr + orig[rand];
            switch (rand) {
                case 0:
                    orig = orig.substr(1, orig.length);
                    break;
                case (orig.length - 1):
                    orig = orig.substr(0, orig.length - 1);
                    break;
                default:
                    orig = orig.substr(0, rand) + orig.substr(rand + 1, orig.length);
                    break;
            }
        }
        return newStr;
    }

    function trimEmpties() {
        // remove any empty array items from beginning & end of array
        if (rand === 0) {
            orig.shift();
        }
        else if (rand === origLength - 1) {
            orig.pop();
        }
    }
}
