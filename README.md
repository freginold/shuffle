# shuffle

shuffle is a JavaScript function to shuffle array elements or characters in a string.  For arrays, the function returns a new array with the same elements as the argument array but in a random order.  For strings, it returns a string containing the same characters but in a random order.

## Usage

There's not much to using shuffle.  First load the file:

```html
<script src="shuffle.js" type="text/javascript"></script>
```

Then call shuffle and assign the value to a variable (or display it, or do whatever you want with it):
```javascript
var shuffledVar = shuffle(originalVar);
```

## Troubleshooting

shuffle has some basic error handling built in.  It will return `false` if an unsupported argument is passed in (such as a number or a non-array object).  There are two error messages and two informational messages that may be logged to the console in certain cases.

#### E1: *Argument is undefined.*

This error is logged when the argument passed in to shuffle has a value of `undefined` (i.e. if you pass in a variable that was declared but never assigned a value).

#### E2: *Argument is not an array or string.*

This error is caused by passing shuffle any argument that is not an array or a string (boolean, number, function, etc.).

#### I1: *Argument is an empty string.*

Self-explanatory; passing shuffle an empty string (`""`) will result in this message being logged.  shuffle will return an empty string in this case.

#### I2: *Argument is an empty array.*

Similarly to the above message, this message will be logged to the console if an empty array (`[]`) is passed in.  The same empty array will be returned.